import { Request, Router } from 'express'
import { getIssues } from '../../controllers/Issues';

export const issuesRouter = Router()

interface IQueryIssue {
  p: number;
}

issuesRouter
  .get('/search', function (req: Request<{},{},{}, IQueryIssue>, res){
    const {
      p
    } = req.query

    console.info(p)

    return res.json(getIssues())
  })

