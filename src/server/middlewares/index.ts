import { Request, Response, NextFunction } from "express";

export function authMiddleware(req: Request, res: Response, next: NextFunction){
  const { authorization } = req.headers;
  
  const isValidAuth = (/Basic .+=/ig).test(authorization ?? '')

  console.log(isValidAuth, authorization)

  if(isValidAuth)
    return next()

  return res
    .status(403)
    .send()
}
