import Express from 'express'
import { logger } from './logger'
import { authMiddleware } from './middlewares'
import cors from 'cors'
import { issuesRouter } from './routes/issue'
import { CORS_GLOBAL_OPTIONS } from '../config/cors'

export const app = Express()

app
  .use(logger)
  .use(cors(CORS_GLOBAL_OPTIONS))
  .use(authMiddleware)
  .use('/api/issues', issuesRouter)
