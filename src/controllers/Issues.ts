import { faker } from '@faker-js/faker'
import { fakeProjects } from '../config/faker'

export function fakeIssue(){
  const startLine = faker.datatype.number()
  const endLine = faker.datatype.number({min:startLine, max:startLine + 2})
  const startOffset = faker.datatype.number()
  const endOffset = faker.datatype.number({min: startOffset + 2, max: startOffset + 200})
  const creationDate = faker.datatype.datetime()

  return {
    key: faker.datatype.uuid(),
    rule: faker.lorem.sentence(),
    severity: faker.helpers.arrayElement(['MINOR', 'MODERATE', 'MAYOR']),
    component: faker.system.filePath(),
    project: faker.helpers.arrayElement(fakeProjects),
    hash: faker.datatype.uuid(),
    textRange:{
      startLine,
      endLine,
      startOffset,
      endOffset
    },
    resolution: faker.helpers.arrayElement(['FIXED', 'PENDING']),
    status: faker.helpers.arrayElement(['CLOSED', 'PENDING']),
    message: faker.lorem.sentence(),
    type: faker.helpers.arrayElement(['CODE_SMELL', 'BUG']),
    scope: faker.helpers.arrayElement(['MAIN']),
    creationDate
  }
}

export function getIssues(customPageIndex?: number){
  const pageIndex = customPageIndex ?? faker.datatype.number()
  const pageSize = 100
  const total = faker.datatype.number()

  return {
    ps: pageSize,
    p: pageIndex,
    total,
    paging:{
      pageIndex,
      pageSize,
      total
    },
    effortTotal: faker.datatype.number(),
    issues: Array.from(Array(pageSize)).map(()=> fakeIssue())

  }
}
