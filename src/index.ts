import { PORT } from "./config/server";
import { app } from "./server";

app.listen(PORT, ()=>{
  console.log('listening at', `http://localhost:${PORT}`)
})
