import { CorsOptions } from "cors";

export const CORS_GLOBAL_OPTIONS: CorsOptions = {
  optionsSuccessStatus: 200,
  origin: /http:\/\/localhost.*/
}
