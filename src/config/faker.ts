import { faker } from "@faker-js/faker"

export const fakeProjects = Object.freeze(Array.from(Array(10)).map(()=>faker.lorem.slug()))
